<h1>Historiques</h1>
<p>
	La Direction des Ressources Humaines du Ministère de l’Economie et des Finances a été créée conformément au décret n° 2011-290 du 12 octobre 2011 portant institution du poste de Directeur des Ressources Humaines dans tous les Ministères.
</p>
<h1>Missions</h1>
<p>
Placée sous la cotutelle du Ministère de l’Economie et des Finances et du Ministère de la Fonction Publique, la Direction des Ressources Humaines est chargée de la gestion des personnels du Ministère de l’Economie et des Finances , constitué des structures suivantes :
<ul>
		<li>Cabinet de Monsieur le Ministre </li>
		<li>Direction des Ressources Humaines (DRH)</li>
		<li>Direction des Affaires Financières et du Patrimoine (DAFP)</li>
		<li>Direction de la Documentation, des Archives et des Publications (DDAP)</li>
		<li>Direction Générale du Trésor et de la Comptabilité Publique (DGTCP)</li>
		<li>Direction Générale de l’Economie (DGE)</li>
		<li>Inspection Générale des Finances (IGF)</li>
</ul>
</p>
<p>
Conformément aux articles 4 et 5 du décret n 2011-290 du 12 octobre 2011 portant institution du poste de Directeur des Ressources Humaines dans tous les Ministères, le Directeur des Ressources Humaines est chargé:
<ul>
	<li>de mettre en œuvre la politique générale de gestion des Ressources Humaines telle que définie par le Ministre en charge de la Fonction Publique ;
	</li>
	<li>d’assurer le suivi de l’application des dispositions relatives à l’éthique et  à la déontologie ;</li>
	<li>d’assurer le suivi de la situation administrative des agents notamment la mise à disposition, la disponibilité, le détachement, le congé,  l’avancement, la promotion ;</li>
	<li>de procéder à l’identification des besoins en formation et au suivi de la mise en œuvre du Plan de Formation du Ministère ;</li>
	<li>d’archiver les actes de gestion du personnel et de tenir à jour le fichier du personnel du Ministère ;</li>
	<li>de créer les conditions de l’amélioration de l’environnement du travail ;</li>
</ul>
<p>Le Directeur des Ressources Humaines rend compte de sa gestion au Ministre technique concerné et au Ministre en charge de la Fonction Publique, par l’établissement d’un bilan semestriel et d’un rapport d’évaluation annuel de ses activités.
</p>
<p>Les missions des Sous-Directions et Services de la Direction des Ressources Humaines sont définies par l’arrêté n° 280 / MEF/ CAB/ DRH du 04 août 2017 portant attributions, organisation et fonctionnement de la Direction des Ressources Humaines du Ministère de l’Economie et des Finances. Ils se présentent comme suit :
</p>
<h1>LES SOUS-DIRECTIONS</h1>
<p>
  La Sous-Direction de la Gestion du Personnel est chargée :
	<ul>
		<li>de la gestion des ressources humaines ;</li>
		<li>de la programmation et du suivi des effectifs ;</li>
		<li>de la gestion prévisionnelle des effectifs ;</li>
		<li>de la gestion prévisionnelle des effectifs ;</li>
		<li>du suivi de la situation administrative des agents (mise à disposition, disponibilité, détachement, avancement, promotion) ;</li>
		<li>de l’archivage des actes de gestion du personnel et de la tenue à jour du fichier du  personnel ;</li>
		<li>de la mise en œuvre de la Politique Sociale ;</li>
		<li>de la sensibilisation des agents sur les droits et prestations sociales ;</li>
		<li>des enquêtes sociales ;</li>
		<li>de la création des conditions de l’amélioration de l’environnement du travail ;</li>
		<li>de l’animation socioculturelle ;</li>
		<li>du conseil, du soutien, de l’assistance et de la veille juridique.</li>
    </ul>


  La Sous-Direction des Etudes et de la Formation est chargée :
<ul>
<li>du recensement des besoins en formation au sein du Ministère;</li>
<li>de la coordination et de la mise en œuvre de la Politique de Formation continue et professionnelle du Ministère ;</li>
	<li>de la gestion de la pédagothèque du Ministère;</li>
<li>de la préparation et de l'organisation des tests et concours éventuels en liaison avec les Services compétents du Ministère chargé de la Fonction Publique;</li>
	<li>de l'élaboration, en collaboration avec les Directions utilisatrices, des
  programmes des enseignements dispensés.</li>
</ul>
<p>
  La Sous-Direction de l'Informatique est chargée :
  <ul>
<li>de la mise en œuvre de l’outil informatique au profit des Sous-Directions et Services du Ministère ;</li>
<li>de la centralisation de la gestion des consommables informatiques de la DRH ;</li>
<li>du développement et de la maintenance du système de gestion intégré des ressources humaines du Ministère ;</li>
<li>de la création, de la mise à jour et de la gestion des bases de données de la DRH ;</li>
<li>de la production des badges des agents du Ministère ;</li>
<li>de la rédaction des manuels d’utilisation des applications développées et de la formation des utilisateurs aux logiciels de la DRH ;</li>
<li>de l’appui technique et du conseil en matière informatique aux Services du Ministère ;</li>
<li>de la maintenance et de la gestion du réseau informatique de la DRH ;</li>
<li>de la maintenance et de la gestion du réseau informatique de la DRH ;</li>
<li>de la maintenance et de la gestion du réseau informatique de la DRH ;</li>
<li>de la veille technologique en matière de matériel et de logiciel informatique.</li>
</ul>
</p>
  <h1>LES SERVICES RATTACHES ET LA CELLULE D’ETUDES </h1>
<p>
  Le Service de l’Ethique et de la Déontologie est chargé :
	<ul>
  <li>du suivi de l’application des dispositions relatives à l’éthique et à la déontologie ;</li>
  <li>de la sensibilisation des agents au respect du code d’éthique et de la déontologie ;</li>
  <li>de la mise en œuvre du code d’éthique et de la déontologie ;</li>
  <li>de l’instauration des contrats d’objectif et de performance dans les structures du Ministère ;</li>
  <li>	de l’évaluation des agents du Ministère ;</li>
  <li>	de prôner la reconnaissance du mérite au sein du Ministère.</li>
  </ul>
</p>
<p>
  Le Service de la Communication est chargé :
	<ul>
  <li>de la gestion des missions à l'étranger au sein du Ministère ;</li>
  <li>de l’élaboration et de la mise en œuvre de la politique de communication de la DRH ;</li>
  <li>de concevoir, organiser et mettre en œuvre la politique de communication de la DRH ;</li>
  <li>de gérer et maîtriser la communication interne et externe ;</li>
  <li>de favoriser l’accès à l’information en vue d’œuvrer pour une meilleure visibilité et pour la notoriété de la DRH ;</li>
  <li>	de coordonner les activités du Service.</li>
  </ul>
</p>
<p>
  Le Service des Affaires Financières est chargé :
	<ul>
  <li>	de l’élaboration et de l’exécution du budget ;</li>
  <li>	de la gestion du patrimoine de la DRH.</li>
  </ul>
</p>
<p>
  Le Service Qualité et Contrôle Interne est chargé :
	<ul>
		<li>de l’élaboration et de la mise en œuvre du système de management de la qualité ;</li>
        <li>de conduire la DRH à la certification ;</li>
        <li>de réaliser les enquêtes de satisfaction des clients de la DRH.</li>
   </ul>

</p>
  <p>
  Le Service Courrier est chargé :
  <ul>
  <li>	d'apporter un appui à la gestion du courrier des Services ;</li>
  <li>	d'assurer la mise à jour de la base de données de gestion du courrier ;</li>
  <li>	d’assurer la distribution du courrier.</li>
  </ul>
  <p>
  La Cellule d’Etudes est chargée :
<ul>
  <li>	d’examiner, d’analyser et de proposer des solutions à certains sujets spécifiques ;</li>
  <li>	d’étudier les dossiers qui lui sont imputés;</li>
  <li>	de rédiger les courriers à la demande du Directeur des Ressources Humaines.</li>
 </ul
</p>
	<h1>Organisation</h1>
	<p>
		La Direction des Ressources Humaines est composée: <br>
		- de  trois Sous-Directions
	<p>
		La Sous-Direction de la Gestion du Personnel qui comprend:
		<ul>
<li>le Service de la Gestion du Personnel ;</li>
<li>le Service de la Planification et des Statistiques ;</li>
<li>le Service des Archives ;</li>
<li>le Service des Actions Sociales ;</li>
<li>le Service des Affaires Juridiques et du Contentieux.</li>
</ul>
	</p>
<p>
	La Sous-Direction des Etudes et de la Formation qui comprend :

<li>le Service de la Formation; </li>
<li>le Service des Etudes ;  </li>
<li>le Service Pédagothèque. </li>
</p>
<p>
La Sous-Direction de l’Informatique qui comprend :
<li>le Service Etudes et Développements ;</li>
<li>le Service Exploitation ;</li>
<li>le Service Maintenance et Réseau.</li>
</p>
 <p> - de cinq Services rattachés :
 <li>le Service de l’Ethique et de la Déontologie ;</li>
<li>le Service de la Communication ;</li>
<li>le Service Qualité et Contrôle Interne ;</li>
<li>le Service des Affaires Financières ;</li>
<li>le Service Courrier.
</p>
<p>
-	D’une Cellule d’Etudes

</p>
