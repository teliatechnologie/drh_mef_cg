<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 //requete de la region
 defined('REGION') OR define('REGION',"SELECT * FROM tbno_notation INNER JOIN tbaf_spref on tbaf_spref.CODE_SPREF = CODE_SP INNER JOIN tbaf_dpmt on tbaf_dpmt.CODE_DPMT = tbaf_spref.CODE_DPMT WHERE tbno_notation.MATRICULE =  ? ");


//requete pour lister les données de la table faq
defined('liste_faq') OR define('liste_faq',"SELECT * FROM faq WHERE active = 1 ORDER BY ordre");

//requete pour lister les emplois
defined('liste_emploi') OR define('liste_emploi',"SELECT * FROM emploi ORDER BY EMP_LIBELLE");

//requete pour lister les types d'actes
defined('liste_acte') OR define('liste_acte',"SELECT * FROM type_acte WHERE etat=1 ORDER BY idtype");
//requete pour lister les types d'actes
defined('type_docs') OR define('type_docs',"SELECT distinct Categorie_Dossier FROM document");

defined('decrets') OR define('decrets',"SELECT pnd.pndId, nom_pnd FROM pnd
inner join article_pnd on article_pnd.pndId=pnd.pndId
group by pnd.pndId, nom_pnd
order by ordre");

defined('test_doc') OR define('test_doc',"SELECT id,nom_document,lien FROM doc_reference WHERE statut = 1 ORDER BY ordre ");
defined('test_doc_acc') OR define('test_doc_acc',"SELECT id,nom_document,lien,date_publication,statut FROM doc_reference WHERE statut = 1 ORDER BY date_publication LIMIT 4");
defined('temoin') OR define('temoin',"SELECT DISTINCT id,libelle FROM naturetem WHERE id=?");
