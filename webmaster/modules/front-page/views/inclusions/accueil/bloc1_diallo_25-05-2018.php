<!--<p>&nbsp;</p>-->
<!-- Actualité et espace fonctionnaire-->
<style type="text/css">
	#btn_bl22{background-color:#CE6200; color:#FFF;}
	#btn_bl22:hover{color:#CE6200; background-color:#ff8b26; border:1px solid #CE6200;}
	.actu_contanier{/* overflow:auto; height:900px;*/}				
	.actualite img{padding:0px; margin:0px; border-spacing:0px}
	.actualite span{color:#333333; text-transform:uppercase; font-weight:bold}
	.resume-actualite{padding:14px; margin-bottom:2px; border-bottom:1px solid #036D00;background-color:#EFEFEF;}	
	.imp{color:#C00}
	.rsond{margin-bottom:5px; padding:6px 10px; background-color:#FFF}
	.bloc{padding:10px; background-color:rgb(59,59,59); color:#FFF;}
	.bloc a{color:#FFF; font-weight:normal; font-family:'Oswald';}
	.bloc input{color:#333;}
	.read-more{font-size:12px}
	.actu{border-top:none}
	
	#concours{
		background-color:#FFF;
		padding:10px;
		box-shadow: 0 0 20px rgba(0, 0, 0, 0.33);	
	}
	
	#concours .titre{	
		background-color:#FFBFBF;
		color:#BF0000;
		padding:10px;
		border-bottom:1px solid #BF0000;
		margin-bottom:30px;
	}
			
	#concours .link_ a{
		color:#666666;
		font-family: "Segoe UI","Segoe WP","Oswald",Arial,sans-serif;
		font-size:15px;
		font-weight:bold;		
	}
	
	#concours .link_ img{
		width:15px;
		height:15px;		
	}
</style>

<div class="panel-grid actu" id="pg-7-3">

<!--bloc 3 gauche-->
<div class="panel-grid-cell" id="pgc-7-3-0">

<div class="so-panel widget widget_text panel-first-child" id="panel-7-3-0-0" data-index="8">
	<div class="textwidget"></div>
</div>

<!--contenu bloc 3 gauche-->
<?php if ($actu_max || $actu_min){ 
	$lien=$lien1=$target=$target1='';			
?>
<div class="so-panel widget widget_black-studio-tinymce widget_black_studio_tinymce panel-last-child" id="panel-7-3-0-1" data-index="9"> 
    
	<h3 class="widget-title">MFP :: ACTUALITES</h3>
    
	<div class="textwidget">      
<!--actualites : contenu prioritaire-->
        <?php if($actu_max) {foreach ($actu_max as $nw){
		$attach=$date_ins=$date_jour=$txt=$datej=$datefin='';	
		$today = explode('/', date('d/m/Y'));
		$fin_ins = explode('/', $nw['date_fin_inscr']);
		
		$datej = $today[2].$today[1].$today[0];
		$datefin = $fin_ins[2].$fin_ins[1].$fin_ins[0];
		
		// date d'insertion et date auj
		$date_jour = date('Y-m-d');	
		$date_ins = $nw['date_ins'];
						
		// gestion des liens
		switch($nw['type_lien']){
				case "auto":
					$lien = $urd.$nw['lien'];
					$target = "_self";
				break;
				
				case "site":
					$lien = $nw['lien'];
					$target = "_blank";
				break;
				
				case "rep":
					$lien = $nw['lien'];
					$target = "_blank";
				break;
				
				case "fichier":
					$lien = $path_fic.$nw['lien'];
					$target = "_blank";
					$attach = "ic_action_attachment_2.png";
				break;
				
				default : 
					$lien = "#";
					$target = "_self";
			}														
		?>
       	<div class="actualite">
      
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
  <!--image des news importantes--> 
  <td width="5%">
  	 <img src="<?php echo base_url('assets/css/icones-rs/new-r.png') ?>" alt="new" width="25" height="25">   
  </td> 
  
    <td width="95%"><span><?php echo $nw['titre'] ?></span></td>  
  </tr>
</table>	  	
      
		
       <div class="resume-actualite imp"><!--contenu FFBFBF-->
        	<?php echo $nw['resume'] ?>
            
      	<p align="right">
        <?php if ($attach){ ?>
        	<img src="<?php echo base_url('assets/css/'.$attach) ?>" alt="pict-actu">
            
               <a href="<?php echo $lien; ?>" target="<?php echo $target;?>" class="read-more">Télécharger</a> 
        <?php }else{?>        
        <a href="<?php echo $lien; ?>" target="<?php echo $target;?>" class="read-more">Lire la suite</a> 
        <?php }?>
                  
        &nbsp;&nbsp;
        
        <?php if(!empty($nw['lien_inscrire'])){
			$today = explode('/', date('d/m/Y'));
		$fin_ins = explode('/', $nw['date_fin_inscr']);
		
		$datej = $today[2].$today[1].$today[0];
		$datefin = $fin_ins[2].$fin_ins[1].$fin_ins[0];
					
			if($datej <= $datefin){							
		?>        	
        	<a href="<?php echo $nw['lien_inscrire']; ?>" target="_blank" class="read-more">s'inscrire</a>
        <?php }}?>
           	&nbsp;
            <?php if(!empty($nw['lien_se_connecter'])){							
		?>
        	<a href="<?php echo $nw['lien_se_connecter']; ?>" target="_blank" class="read-more">se connecter</a>
            <?php }?>
        </p>             
        </div>                 
        
        <div class="date_actu" align="left">Publié le&nbsp;<?php echo $nw['date_ins'] ?></div>
        
        </div>      
        <?php }}?>
        <!--actualites prioritaires-->
          
          
          <!--actualites : -->
        <?php if($actu_min) {foreach ($actu_min as $nw1){
			$attach1=$date_ins1=$date_jour1=$txt1=$datej1=$datefin1='';	
	
		
		// date d'insertion et date auj
		$date_jour1 = date('Y-m-d');	
		$date_ins1 = $nw1['date_ins'];
		
			// gestion des liens
		switch($nw1['type_lien']){
				case "auto":
					$lien1 = $nw1['lien'];
					$target1 = "_self";
				break;
				
				case "site":
					$lien1 = $nw1['lien'];
					$target1 = "_blank";
				break;
				
				case "rep":
					$lien1 = '/'.$nw1['lien'];
					$target1 = "_blank";
				break;
				
				case "fichier":
					$lien1 = $path_fic.$nw1['lien'];
					$target1 = "_blank";
					$attach1 = "ic_action_attachment_2.png";
				break;
				
				default : 
					$lien1 = "#";
					$target1 = "_self";
			}														
		?>
        	<div class="actualite">
      
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>  
  <td width="5%">          
      <?php if ($date_ins1==$date_jour1){ ?> 
  	 <img src="<?php echo base_url('assets/css/icones-rs/new-y.png') ?>" alt="new" width="30" height="30">
  <?php }else{?>
   <img src="<?php echo base_url('assets/css/ic_action_bookmark.png') ?>" alt="pict-actu">
  <?php }?>  
  </td> 
  
    <td width="95%">&nbsp;<span><?php echo $nw1['titre'] ?></span></td>
  </tr>
</table>	  	      
		
       <div class="resume-actualite"><!--contenu-->
        	<?php echo $nw1['resume'] ?>
            
      	<p align="right">
        <?php if ($attach1){ ?>
        	<img src="<?php echo base_url('assets/css/'.$attach1) ?>" alt="pict-actu">
             <a href="<?php echo $lien1; ?>" target="<?php echo $target1;?>" class="read-more">Télécharger</a> 
        <?php }else{?>        
        <a href="<?php echo $lien1; ?>" target="<?php echo $target1;?>" class="read-more">Lire la suite</a> 
        <?php }?>         
        &nbsp;&nbsp;
        
            <?php if(!empty($nw1['lien_inscrire'])){										
		$today1 = explode('/', date('d/m/Y'));
		$fin_ins1 = explode('/', $nw1['date_fin_inscr']);
		
		$datej1 = $today1[2].$today1[1].$today1[0];
		$datefin1 = $fin_ins1[2].$fin_ins1[1].$fin_ins1[0];			
			if($datej1 <= $datefin1){				
		?>        	
        	<a href="<?php echo $nw1['lien_inscrire']; ?>" target="_blank" class="read-more">s'inscrire</a>
        <?php }}?>
        
           &nbsp;
           
            <?php if(!empty($nw1['lien_se_connecter'])){							
		?>
        	<a href="<?php echo $nw1['lien_se_connecter']; ?>" target="_blank" class="read-more">se connecter</a>
            <?php }?>
        </p>
        </div>       
        
        <div class="date_actu" align="left">Publié le&nbsp;<?php echo $nw1['date_ins'] ?></div>
                       	           
</div>      
        <?php }}?>
        <!--actualites-->
        
        <p align="right">
        	<a href="<?php echo $urd.'archives/_actualite' ?>" class="btn btn-primary">Voir plus</a>
        </p>  
	</div>  

</div>
</div>
<!--fin du bloc 3 gauche-->
<?php }?>

<!--Espace fonctionnaire-->
<div class="panel-grid-cell" id="pgc-7-3-1">

<div class="so-panel widget widget_text panel-first-child" id="panel-7-3-1-0" data-index="10">
	<div class="textwidget"></div>
</div>

<div class="so-panel widget widget_black-studio-tinymce widget_black_studio_tinymce panel-last-child" id="panel-7-3-1-1" data-index="11">

<!--lucarne concours 20xx-->
	<h3 class="widget-title" id="ep">CONCOURS <?php echo date('Y'); ?></h3>
    
    <div class="textwidget">    	        
     <div id="concours">
<div class="titre"><strong><span>URGENT</span></strong><br>
Il est ouvert au titre de l'année 2018, des Concours Administratifs et des Personnels de Santé.</div>

 <!-- <ul>
    <li><a href="https://fonctionpublique.laatech.net/2018/accueil" target="_blank">Inscription et Communiqués</a></li>
 
    <li><a href="https://fonctionpublique.laatech.net/2018/moncompte/seconnecter" target="_blank">Connexion espace candidat</a></li>
 
    <li><a href="https://web.facebook.com/Fonctionpublique.ci/photos/pcb.1640111736035925/1640110712702694/?type=3&theater" target="_blank">Pièces à fournir et Chronogramme</a></li>
  </ul>-->
  
  <table width="85%" border="0" align="center" cellpadding="7" cellspacing="3" class="link_">
  <tr>
    <td width="8%"><img src="<?php echo base_url() ?>assets/css/icones-rs/ic_action_goright.png" alt=""></td>
    <td width="92%"><a href="https://fonctionpublique.laatech.net/2018" target="_blank">Inscription et Communiqués</a></td>
  </tr>
  <tr>
    <td><img src="<?php echo base_url() ?>assets/css/icones-rs/ic_action_goright.png" alt=""></td>
    <td><a href="https://fonctionpublique.laatech.net/2018/moncompte/seconnecter.php" target="_blank">Connexion espace candidat</a></td>
  </tr>
  <tr>
    <td><img src="<?php echo base_url() ?>assets/css/icones-rs/ic_action_goright.png" alt=""></td>
    <td><a href="https://www.fonctionpublique.gouv.ci/index.php/front-page/navigator/statics/menu/recrutement/calendrier" target="_blank">Chronogramme et Pièces à fournir </a></td>
  </tr>
  <tr>
    <td><img src="<?php echo base_url() ?>assets/css/icones-rs/ic_action_goright.png" alt=""></td>
    <td><a href="https://fonctionpublique.laatech.net/2018/moncompte/reset.php" target="_blank">Mot de passe oublié</a></td>
  </tr>
  <tr>
    <td><img src="<?php echo base_url() ?>assets/css/icones-rs/ic_action_goright.png" alt=""></td>
    <td><a href="https://fonctionpublique.laatech.net/2018/recuperationpaiement.php" target="_blank">Récupérer un paiement</a></td>
  </tr>
  <tr>
    <td><img src="<?php echo base_url() ?>assets/css/icones-rs/ic_action_goright.png" alt=""></td>
    <td><a href="https://fonctionpublique.laatech.net/formation18/" target="_blank">Cours de préparation</a></td>
  </tr>
<!--  <tr>
    <td><img src="<?//php echo base_url() ?>assets/css/icones-rs/ic_action_goright.png" alt=""></td>
    <td><a href="https://fonctionpublique.laatech.net/2018/inscriptionSante/etape/1" target="_blank">Inscription  Concours  Agents de Santé</a></td>
  </tr>-->
<!--  <tr>
    <td><img src="<?php //echo base_url() ?>assets/css/icones-rs/ic_action_goright.png" alt=""></td>
    <td><a href="#">Résultats Concours 2018</a></td>
  </tr>-->
  </table>

  <p>&nbsp;</p>
  <table width="100%" border="0" cellspacing="4" cellpadding="4">
  <tr>
    <td><strong>Délai d'inscription</strong><br>
<span style="color:red;">Du vendredi 09 mars au vendredi 18 mai 2018</span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    </tr>
  <tr>
    <td><strong>Infoline</strong><br>
01 16 16 92 / 06 06 06 81 / 07 07 29 91 / 57 57 49 49 (De 07h30 à 16h30)</td>
  </tr>
</table>

	</div>             
	</div>
<!--lucarne concours 20xx--> 
<p>&nbsp;</p>
<!--espace fonctionnaire-->
	<h3 class="widget-title" id="ep">Espace Fonctionnaire</h3>
    
    <div class="textwidget">
    	<?php 
	if(!empty($msg) && $this->uri->segment(3)=='connexion'){if($type==1){$msg_type = 'msg-erreur';}else{$msg_type = 'msg-succes';}
?>
        <div class="msg <?php echo $msg_type; ?>" id="esp">
            <?php echo $msg; ?>
        </div>
        
         <script type="text/javascript"> 
    	setTimeout(function(){
    		document.getElementById('esp').style.display = 'none';
    },15000);
    </script>
<?php }?>
        
     <div id="connexion" class="esp_fon bloc">
		<form action="<?php echo site_url($ctrl.'/connexion#ep') ?>" method="post" name="form_espfon">
          	<table width="100%" border="0" cellspacing="5" cellpadding="7">                            
                            
               <tr>              
                <td colspan="2" align="center"><input name="matricule" type="text" autocomplete="off" required class="champ_de_saisie" placeholder="Entrez votre matricule" maxlength="15" style="text-transform:uppercase" /></td>
              </tr>
              
              <tr><td colspan="2">&nbsp;</td></tr>
            
              <tr>             
                <td colspan="2" align="center"><input name="mot_de_passe" id="mot_de_passe" autocomplete="off" required type="password" class="champ_de_saisie" placeholder="Entrez votre mot de passe" /></td>
              </tr>      
                      
              <tr><td colspan="2">&nbsp;</td></tr>
              
              <tr>
                <td colspan="2" align="center"><input type="submit" name="button" id="" value="CONNEXION" class="btn btn-primary" /></td>
              </tr>              
              <tr><td colspan="2">&nbsp;</td></tr>
              
              <tr valign="middle">
                <td width="11%">   <img src="<?php echo base_url('assets/css/icones-rs/ic_chevron_right_white_24dp.png') ?>" alt="pict-actu">
                 </td>
                <td width="89%"><a href="<?php echo site_url($ctrl.'/inscription') ?>">s'inscrire</a></td>                                 
              </tr>              
              <tr valign="middle"><td>   <img src="<?php echo base_url('assets/css/icones-rs/ic_chevron_right_white_24dp.png') ?>" alt="pict-actu"></td>
                <td><a href="<?php echo site_url($ctrl.'/password') ?>">mot de passe oublié ?</a></td>
              </tr>
              <tr>
                <td colspan="2">&nbsp;</td>
              </tr>
              
            </table>
            <input name="cc" type="hidden" id="cc" value="sdf" />
            <input name="parent" type="hidden" id="parent" value="accueil" />
        </form>
	</div>             
	</div>
<!--espace fonctionnaire--> 

<!--sondage-->
<p>&nbsp;</p>   
<h3 class="widget-title" id="snd">Sondage</h3>

      <?php 
	if(!empty($msg) && $this->uri->segment(3)=='vote_sondage'){if($type==1){$msg_type = 'msg-erreur';}else{$msg_type = 'msg-succes';}
?>
    <div class="msg <?php echo $msg_type; ?>" id="sond">
    	<?php echo $msg; ?>
    </div>
    
    <script type="text/javascript"> 
    	setTimeout(function(){
    		document.getElementById('sond').style.display = 'none';
    },15000);
    </script>
<?php }?> 
                    
   		<div class="esp_fon bloc">        	
                <p>
              		<?php echo $sondage['contenu'] ?>
              </p>       
        
        <p>
       	  <form action="<?php echo site_url($ctrl.'/vote_sondage#snd') ?>" method="post">
            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr><td colspan="2">&nbsp;</td></tr>
                    
                    <tr>
                    	<td width="23%">&nbsp;</td>
                    	<td width="77%"> (<?php echo $sondage['nbp'] ?>&nbsp;votes)</td>
                        <input name="id_sond" type="hidden" value="<?php echo $sondage['id'] ?>">
                         <input name="nbp" type="hidden" value="<?php echo $sondage['nbp'] ?>">
                    </tr>
                    
                    <?php 
					// reponse
					$reponse = $this->$model->read
					(
						'_reponse',
						array('groupe' => $sondage['group_reponse'])
					);									
				
					foreach($reponse as $r_sond){?>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    
                     <tr>
                    	<td width="23%">&nbsp;</td>
                    	<td width="77%"><input name="radio" type="radio" value="<?php echo $r_sond['id'].'-'.$r_sond['choix'] ?>" />&nbsp;<?php echo $r_sond['reponse'] ?>&nbsp;<span class="">(<?php echo $r_sond['choix'] ?>)</span></td>
                    </tr>
                <?php }?>   
            </table>  
                 
                 <br /><br />
                 
             <!--résultat du sondage-->
             <?php 
			 	// resultat
				/*	$resultat = $this->$model->read
					(
						'_reponse',
						array('groupe' => $sondage['group_reponse'])
					);*/
			 ?>
            <!-- <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" id="rsond" class="rsond">
              <tr>
                <td colspan="2" class="t_rsond">Résultat du sondage</td>              
              </tr>
              <tr>
                <td colspan="2">&nbsp;</td>               
              </tr>-->
              <?php 
			  //foreach($resultat as $res){?>                                      
                  <!--   <tr>
                    	<td width="59%" align="right" class="c_rsond"><?php //echo $res['reponse'] ?></td>                      
                    	<td width="41%" class="c_rsond">&nbsp;<?php //echo $res['choix'] ?></td>
                    </tr>-->
                <?php //}?> 
             <!-- <tr>
                <td colspan="2">&nbsp;</td>            
              </tr>
            </table>-->

             <!---->
                 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="34%" align="left"><input name="votez" type="submit" value="Votez" id="votez" class="btn btn-primary" /></td>
                    <!--<td width="33%">&nbsp;&nbsp;<a href="javascript:void(0);" onClick="showResult()">Résultat</a></td>-->
                    <td width="33%">&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="3" align="center">&nbsp;</td>                  
                  </tr>
            </table>                           
       	  </form>
        </p>
        </div>
<!--sondage-->

<!--newsletter-->
<p>&nbsp;</p>
<h3 class="widget-title" id="nl">NEWSLETTER</h3>
        <?php 
	if(!empty($msg) && $this->uri->segment(3)=='newsletter'){if($type==1){$msg_type = 'msg-erreur';}else{$msg_type = 'msg-succes';}
?>
    <div class="msg <?php echo $msg_type; ?>" id="nlt">
    	<?php echo $msg; ?>
    </div>
    
    <script type="text/javascript"> 
    	setTimeout(function(){
    		document.getElementById('nlt').style.display = 'none';
    },15000);
    </script>
<?php }?>  
            <div class="esp_fon bloc">
        <p>
         Inscrivez-vous à notre newsletter pour être toujours bien informé ! 
        </p>
        
        <p>
       	  <form action="<?php echo site_url($ctrl.'/newsletter#nl') ?>" method="post">
            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr><td>&nbsp;</td></tr>                                        
                    
                    <tr>                    	
                    	<td align="center"><input name="mail_nl1" type="email" required placeholder="Entrez votre Email" class="champ_de_saisie" /></td>
                    </tr>
                    <tr><td>&nbsp;</td></tr>
                    
                    <tr>                    	
                    	<td align="center"><input name="mail_nl2" type="email" required placeholder="Confirmez votre Email" class="champ_de_saisie" /></td>
                    </tr>
                    <tr><td>&nbsp;</td></tr>                                                       
            
                  <tr>
                    <td align=""><input name="btn_nl" id="btn_nl" type="submit" value="JE M'INSCRIS" class="btn btn-primary" /></td>
                  </tr>
                
            </table>                           
       	  </form>
        </p>
        </div> 
<!--newsletter-->
    
</div>
</div>
<!--fin bloc 3 droit-->

</div>

<script type="text/javascript">
  function showResult(){
	if(document.getElementsByClassName('rsond').style.display=="block"){
		document.getElementsByClassName('rsond').style.display="none";	
	}
	else{
		document.getElementsByClassName('rsond').style.display="block";
	}
	//$('#rsond').show();
  }
</script>
